
import {
  GET_ACTIONS,
  GET_MY_ACTIONS,
  ADD_ACTION,
  DELETE_ACTION,
  GET_ACTION_DETAIL,
  GET_ACTION_DETAIL_REAL_TIME
} from '../../../constants';

const INITIAL_STATE = {
  isFetching: false,
  didInvalidate: false,
  info: {},
  realTime: []
};

const twelveDataReducer = (state = INITIAL_STATE, action) => {

  switch (action.type) {
    case `${GET_ACTIONS}_REJECTED`:
      return {
        ...state,
        isFetching: false,
        didInvalidate: true,
      };
    case `${GET_ACTIONS}_PENDING`:
      return {
        ...state,
        isFetching: true,
        didInvalidate: false,
      };
    case `${GET_ACTIONS}_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        didInvalidate: false,
        info: action.payload.request.data,
      };

    case `${ADD_ACTION}_REJECTED`:
      return {
        ...state,
        isFetching: false,
        didInvalidate: true,
      };
    case `${ADD_ACTION}_PENDING`:
      return {
        ...state,
        isFetching: true,
        didInvalidate: false,
      };
    case `${ADD_ACTION}_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        didInvalidate: false,
        myActions: action.payload.myActions,
      };
    case `${GET_MY_ACTIONS}_REJECTED`:
      return {
        ...state,
        isFetching: false,
        didInvalidate: true,
      };
    case `${GET_MY_ACTIONS}_PENDING`:
      return {
        ...state,
        isFetching: true,
        didInvalidate: false,
      };
    case `${GET_MY_ACTIONS}_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        didInvalidate: false,
        myActions: action.payload.myActions,
      };
    case `${DELETE_ACTION}_REJECTED`:
      return {
        ...state,
        isFetching: false,
        didInvalidate: true,
      };
    case `${DELETE_ACTION}_PENDING`:
      return {
        ...state,
        isFetching: true,
        didInvalidate: false,
      };
    case `${DELETE_ACTION}_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        didInvalidate: false,
        myActions: action.payload.myActions,
      };

    case `${GET_ACTION_DETAIL}_REJECTED`:
      return {
        ...state,
        isFetching: false,
        didInvalidate: true,
      };
    case `${GET_ACTION_DETAIL}_PENDING`:
      return {
        ...state,
        isFetching: true,
        didInvalidate: false,
      };
    case `${GET_ACTION_DETAIL}_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        didInvalidate: false,
        myActions: action.payload.myActions,
      };
      // case `${GET_ACTION_DETAIL_REAL_TIME}_REJECTED`:
      //   return {
      //     ...state,
      //     isFetching: false,
      //     didInvalidate: true,
      //   };
      // case `${GET_ACTION_DETAIL_REAL_TIME}_PENDING`:
      //   return {
      //     ...state,
      //     isFetching: true,
      //     didInvalidate: false,
      //   };
      // case `${GET_ACTION_DETAIL_REAL_TIME}_FULFILLED`:
      //   return {
      //     ...state,
      //     isFetching: false,
      //     didInvalidate: false,
      //     myActions: action.payload.myActions,
      //   };
  





    default:
      return state;
  }
};

export { twelveDataReducer };
