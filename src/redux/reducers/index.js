import { combineReducers } from 'redux';

import { autheticationReducer } from './authentication';
import { twelveDataReducer} from './twelveData'

const rootReducer = combineReducers({
  autheticationReducer,
  twelveDataReducer
})

export { rootReducer }
