import axios from 'axios';

import {
  LOGIN_USER,
  SIGNOUT_USER
} from '../../../constants';

const INITIAL_STATE = {
  isFetching: false,
  didInvalidate: false,
  user: {},
  token: false
};

const autheticationReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {

    case `${LOGIN_USER}_REJECTED`:
      return {
        ...state,
        isFetching: false,
        didInvalidate: true,
      };
    case `${LOGIN_USER}_PENDING`:
      return {
        ...state,
        isFetching: true,
        didInvalidate: false,
      };
    case `${LOGIN_USER}_FULFILLED`:
      // axios.defaults.headers = {
      //   "auth-token": action.payload.token,
      // }
      return {
        ...state,
        isFetching: false,
        didInvalidate: false,
        user: action.payload.user,
        token: action.payload.token,
      };
    case `${SIGNOUT_USER}_REJECTED`:
      return {
        ...state,
        isFetching: false,
        didInvalidate: true,
      };
    case `${SIGNOUT_USER}_PENDING`:
      return {
        ...state,
        isFetching: true,
        didInvalidate: false,
      };

    case `${SIGNOUT_USER}_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        didInvalidate: false,
        user: action.payload,
        token: false
      };


    default:
      return state;
  }
};

export { autheticationReducer };
