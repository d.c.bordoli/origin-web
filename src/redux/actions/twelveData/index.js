import {
  GET_ACTIONS,
  GET_MY_ACTIONS,
  ADD_ACTION,
  DELETE_ACTION,
  GET_ACTION_DETAIL,
  GET_ACTION_DETAIL_REAL_TIME,
  GET_ACTION_DETAIL_HISTORICAL
} from '../../../constants';
import { infoApiService, addMyAction, getMyActionsService, deleteActionService, getActionDetailService, getActionDetailRealTimeService, getActionDetailHistoricalService } from '../../../services'

export const getActions = () => ({
  type: GET_ACTIONS,
  payload: infoApiService()
})

export const getMyActions = () => ({
  type: GET_MY_ACTIONS,
  payload: getMyActionsService()
})


export const addAction = (myAction) => ({
  type: ADD_ACTION,
  payload: addMyAction(myAction)
})

export const deleteAction = (symbol) => ({
  type: DELETE_ACTION,
  payload: deleteActionService(symbol)
})

export const getActionDetail = (symbol) => ({
  type: GET_ACTION_DETAIL,
  payload: getActionDetailService(symbol)
})

export const getActionDetailRealTime = (symbol, interval) => ({
  type: GET_ACTION_DETAIL_REAL_TIME,
  payload: getActionDetailRealTimeService(symbol, interval)
})

export const getActionDetailHistorical = (symbol, interval, startDate, endDate) => ({
  type: GET_ACTION_DETAIL_HISTORICAL,
  payload: getActionDetailHistoricalService(symbol, interval, startDate, endDate)
})








