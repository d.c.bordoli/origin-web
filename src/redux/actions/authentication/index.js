import {
  LOGIN_USER,
  SIGNOUT_USER,
  CREATE_USER,
} from '../../../constants';
import { loginUserService, createUserService, logoutUserService } from '../../../services'

export const loginUser = (credentials) => ({
  type: LOGIN_USER,
  payload: loginUserService(credentials)
})

export const signoutUser = () => ({
  type: SIGNOUT_USER,
  payload: logoutUserService()
})

export const createUser = (user) => ({
  type: CREATE_USER,
  payload: createUserService(user)
})

