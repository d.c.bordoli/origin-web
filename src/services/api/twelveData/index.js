import axios from "axios";

export const infoApiService = async () => {
  const { data } = await axios.get('/user/info')

  try {
    console.log('data', data)
    return data;

  } catch (e) {
    return e;
  }
};

export const getMyActionsService = async () => {
  const { data } = await axios.get('/user/action/myactions')

  try {
    console.log('data', data)
    return data;

  } catch (e) {
    console.log('error', e)
    return e;
  }
};


export const addMyAction = async (myAction) => {
  const { data } = await axios.post('/user/action/add', myAction)

  try {
    console.log('data', data)
    return data;

  } catch (e) {
    return e;
  }
};

export const deleteActionService = async (symbol) => {
  const { data } = await axios.post(`/user/action/delete/${symbol}`)

  try {
    console.log('data', data)
    return data;

  } catch (e) {
    return e;
  }
};

export const getActionDetailService = async (symbol) => {
  const { data } = await axios.get(`/user/action/detail/${symbol}`)

  try {
    console.log('data', data)
    return data;

  } catch (e) {
    return e;
  }
};

export const getActionDetailRealTimeService = async (symbol,interval) => {
  const { data } = await axios.get(`/user/action/detail/realtime/${symbol}/${interval}`)

  try {
    console.log('data', data)
    return data;

  } catch (e) {
    return e;
  }
};

export const getActionDetailHistoricalService = async (symbol, startDate, endDate) => {
  const { data } = await axios.get(`/user/action/detail/historical/${symbol}/${startDate}/${endDate}`)

  try {
    console.log('data', data)
    return data;

  } catch (e) {
    return e;
  }
};







