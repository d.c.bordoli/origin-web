import axios from "axios";

import { config } from "../../config";

export { loginUserService, createUserService, logoutUserService } from "./authentication";
export { infoApiService, addMyAction, getMyActionsService, deleteActionService, getActionDetailService, getActionDetailRealTimeService, getActionDetailHistoricalService} from "./twelveData"
axios.defaults.baseURL = config.API_URL;

