import axios from "axios";

export const loginUserService = async (credentials) => {
  const { data } = await axios.post('/user/login', credentials)

  try {
    console.log('data', data)
    return data;

  } catch (e) {
    return e;
  }
};

export const createUserService = async (user) => {
  const { data } = await axios.post('/user/register', user);

  try {
    return data;

  } catch (e) {
    return e;
  }
};

export const logoutUserService = async () => {
  const { data } = await axios.get('/user/logout');

  try {
    return data;

  } catch (e) {
    return e;
  }
};


