const styles = {
  input: {
    width: 392,
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    color: '#666666',
    padding: 20
  },
}

export { styles }