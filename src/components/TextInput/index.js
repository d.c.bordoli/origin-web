import { styles } from "./styles";

const TextInput = ({ name, type, register, defaultValue, placeholder, customStyles, disabled }) => (
  <input
    defaultValue={defaultValue}
    name={name}
    placeholder={placeholder}
    type={type || "text"}
    {...register}
    disabled={disabled}
    style={customStyles || styles.input}
  />
);

export { TextInput };
