import React from "react";

const Button = ({ text, onClick, styles, disabled }) => (
  <button disabled={disabled} onClick={onClick} style={styles}>
    {text}
  </button>
);

export { Button };
