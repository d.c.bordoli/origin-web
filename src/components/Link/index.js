import React from "react";

import { styles } from "./styles";

const Link = ({ title, onClick, color }) => {
  const customStyles = styles(color);

  return (
    <p onClick={onClick} style={customStyles.text}>{title}</p>
  )
};

export { Link };