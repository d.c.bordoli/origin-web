const styles = (color) => ({
  text: {
    fontFamily: 'Montserrat',
    fontSize: 14,
    color: color,
    borderBottom: `1px solid ${color}`,
    cursor: 'pointer',
  }
});

export { styles };
