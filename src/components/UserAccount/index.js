import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { signoutUser } from './../../redux/actions'
import { useHistory } from 'react-router-dom';



import { styles } from "./styles";

const UserAccount = () => {
  const dispatch = useDispatch()
  const history = useHistory();

  const { user: { userName } } = useSelector((state) => state.autheticationReducer);

  const handleLogout = () =>{
    dispatch(signoutUser())
      .then(response => history.push('/login'))
  }

  return (
    <div style={styles.container}>
      <div>
        <p style={styles.userName}>Usuario:    {userName}</p>
      </div>

      <div>
        <p style={styles.logout} onClick={handleLogout}>Logout</p>
      </div>

    </div>
  )
};

export { UserAccount };