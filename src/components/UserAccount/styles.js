const styles = {
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    margin: '0px 40px',
    cursor: 'pointer'
  },
  userName: {
    fontSize: 15,
    fontFamily: 'Montserrat',
    fontWeight: 700,
    textTransform: 'uppercase',
    color: '#858BA6',
    marginRight: 30
  },
  logout: {
    fontSize: 15,
    fontFamily: 'Montserrat',
    fontWeight: 700,
    textTransform: 'uppercase',
    color: 'white',
    margin: 0

  }
}

export { styles };