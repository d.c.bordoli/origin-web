import React, { useMemo } from "react";
import { useTable } from 'react-table';
import { useHistory } from 'react-router-dom';

import { Button } from './../../components'
import { deleteAction } from './../../redux/actions'

import { useDispatch } from 'react-redux';

import { styles } from './styles';

const Table = ({ dataTable }) => {
  const dispatch = useDispatch()
  const history = useHistory();
  const data = useMemo(() => dataTable, []);

  const handleClick = (data) => {
    dispatch(deleteAction(data))
      .then(response => window.location.reload())
  }

  const columns = useMemo(
    () => [
      {
        Header: 'Simbolo',
        accessor: 'symbol', // accessor is the "key" in the data
        Cell: function orderItems({ row }) {
          return (
            <p style={{ cursor: 'pointer' }} onClick={() => history.push(`/actiondetail/${row.original.symbol}`)} >{row.original.symbol}</p>
          )

        }
      },
      {
        Header: 'Nombre',
        accessor: 'name',
      },
      {
        Header: 'Moneda',
        accessor: 'currency',
      },
      {
        Header: 'Eliminar',
        Cell: function orderItems({ row }) {
          return (
            <Button styles={styles.button} text="Eliminar" onClick={() => handleClick(row.original.symbol)} />
          )
        }
      },

    ],
    []
  )


  const tableInstance = useTable({ columns, data: dataTable })
  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow, } = tableInstance

  return (
    <table {...getTableProps()} style={styles.container}>
      <thead style={{ display: 'flex', flex: 1 }}>
        {headerGroups.map((headerGroup, i) => (
          <tr {...headerGroup.getHeaderGroupProps()} key={i} style={{ display: 'flex', flex: 1, padding: 20 }}>
            {headerGroup.headers.map((column, i) => (
              <th
                key={i}
                {...column.getHeaderProps()}
                style={{ display: 'flex', flex: 1, fontSize: 12, fontFamily: 'Montserrat', fontWeight: 700, color: '#858BA6' }}
              >
                {column.render('Header')}
              </th>
            ))}
          </tr>
        ))}
      </thead>

      <tbody {...getTableBodyProps()} style={{ display: 'flex', flex: 1, flexDirection: 'column' }}>
        {rows.map((row, i) => {
          prepareRow(row)
          return (
            <tr {...row.getRowProps()} key={i} style={styles.rowDark}
            >
              {row.cells.map((cell, i) => {
                return (
                  <td key={i} {...cell.getCellProps()} style={{ display: 'flex', flex: 1 }}>
                    {cell.render('Cell')}
                  </td>
                )
              })}
            </tr>
          )
        })}
      </tbody>
    </table>)

}

export { Table };