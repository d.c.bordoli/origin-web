const styles = {
  container: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    borderRadius: 10,
    backgroundColor: '#2F3347'
  },
  rowDark: {
    display: 'flex',
    flex: 1,
    backgroundColor: '#2A2D3E',
    color: '#E4E3F1',
    fontSize: 11,
    fontFamily: 'Montserrat',
    fontWeight: 700,
    textTransform: 'uppercase',
    padding: 20,
    borderRadius: 10
  },
  rowLight: {
    display: 'flex',
    flex: 1,
    backgroundColor: '#2F3347',
    color: '#E4E3F1',
    fontSize: 11,
    fontFamily: 'Montserrat',
    fontWeight: 700,
    textTransform: 'uppercase',
    padding: 20,
    borderRadius: 10
  },
  button: {
    width: 133,
    height: 31,
    borderRadius: 10,
    backgroundColor: '#17A8B0',
    color: '#FFFFFF',
    fontWeight: 700,
    fontFamily: 'Arial',
    cursor: 'pointer',
  },

};

export { styles };