import React from 'react'
import { useForm } from "react-hook-form";
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useAlert } from 'react-alert';

import { styles } from './styles'
import { Button, Link, TextInput } from '../../../components'
import { loginUser } from '../../../redux/actions'

const LoginView = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { register, handleSubmit } = useForm();
  const alert = useAlert();
  const { isFetching } = useSelector((state) => state.autheticationReducer)

  const onSubmit = (credentials) => {
    dispatch(loginUser(credentials))
    .catch(() => alert.show('Usuario o clave invalida'))

  };

  return (
    <div style={styles.container}>

      <div style={styles.form}>
        <p style={styles.title}>Login</p>
        <p style={styles.subtitle}>Ingresa a nuestro sitio</p>


        <TextInput name="userName" placeholder="usuario" register={{ ...register("userName", { required: true }) }} />
        <TextInput name="password" placeholder="********" register={{ ...register("password", { required: true }) }} type="password" />
        <Button disabled={isFetching} onClick={handleSubmit(onSubmit)} styles={styles.button} text="LOGIN" />

        <div style={styles.line} />
        <Link color="#46c2ab" onClick={() => history.push('/register')} title="Registrarse" />
      </div>
    </div>
  )
};

export { LoginView };