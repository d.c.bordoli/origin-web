import React from 'react'
import { useForm } from "react-hook-form";
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import { useAlert } from 'react-alert';

import { styles } from './styles'
import { Button, Link, TextInput } from '../../../components'
import { createUser } from '../../../redux/actions'


const RegisterView = () => {
  const dispatch = useDispatch()
  const history = useHistory();
  const { register, handleSubmit, getValues, formState: { errors } } = useForm();
  const alert = useAlert();

  const onSubmit = (data) => {
    dispatch(createUser(data))
      .then((response)=>history.push('/dashboard'))
      .catch((err) => alert.show('Un error ocurrió', err))
  }


  return (
    <div style={styles.container}>

      <div style={styles.form}>
        <p style={styles.title}>Registro</p>
        <p style={styles.subtitle}>CREA UNA CUENTA</p>

        <TextInput name="userName" placeholder="Nombre de usuario" register={{ ...register("userName", { required: true }) }} />
        <TextInput name="password" placeholder="Contraseña" register={{ ...register("password", { required: true }) }} type="password" />
        <TextInput name="passwordConfirm" placeholder="Confirma tu contraseña" register={{ ...register("passwordConfirm", {required: true})}} type="password" />

        <Button onClick={handleSubmit(onSubmit)} styles={styles.button} text="Crear cuenta" />
        <Link color="#cb76df" onClick={() => history.push('/login')} title="Ya tengo una cuenta" />

      </div>
    </div>
  )

}
export { RegisterView }