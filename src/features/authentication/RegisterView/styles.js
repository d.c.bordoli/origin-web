
const styles = {
  button: {
    width: 433,
    height: 61,
    borderRadius: 30,
    backgroundColor: '#47C8B0',
    color: '#FFFFFF',
    fontWeight: 700,
    fontFamily: 'montserrat',
    cursor: 'pointer'
  },
  container: {
    display: "flex",
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    //background: 'linear-gradient(298.13deg, #2F3347 6.04%, #212332 53.73%)',
    overflow: 'scroll'
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    width: 535,
    height: 782,
    borderRadius: 10,
    backgroundColor: '#2A2D3E'
  },
  logo: {
    width: 243,
    height: 72,
    margin: 40,
  },
  text: {
    fontSize: 14,
    fontFamily: 'montserrat',
    color: '#858BA6'
  },
  title: {
    fontSize: 30,
    fontWeight: 700,
    fontFamily: 'montserrat',
    color: '#E4E3F1',
  },
  subtitle: {
    fontSize: 14,
    fontWeight: 700,
    fontFamily: 'montserrat',
    color: '#858BA6',
  }
}

export { styles }