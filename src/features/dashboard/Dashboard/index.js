import React, { useEffect, useState } from 'react';
import { styles } from './styles';
import { TextInput, Table, UserAccount, Button } from '../../../components'
import { getActions, addAction, getMyActions } from './../../../redux/actions'
import { useDispatch } from 'react-redux';
import { useForm } from 'react-hook-form'





const Dashboard = () => {
  const [downloadData, setDownloadData] = useState([])
  const [text, setText] = useState('')
  const [suggestions, setSuggestions] = useState([])
  const [myActions, setMyActions] = useState([])
  const [infoComplete, setInfoComplete] = useState({ symbol: '', name: '', currency: '' })

  const [dataTable, setDataTable] = useState([])


  const { register, errors, handleSubmit } = useForm({
    reValidateMode: 'onSubmit'
  })


  const dispatch = useDispatch()



  const handleChange = (text) => {
    let matches = []
    if (text.length > 0) {
      matches = downloadData.filter(data => {
        const regex = new RegExp(`${text}`, "gi")
        return data.name.match(regex) || data.symbol.match(regex) || data.currency.match(regex)
      })
    }
    setSuggestions(matches)
    setText(text)
  };

  const handleSuggestions = (text) => {
    setInfoComplete({ symbol: text.symbol, name: text.name, currency: text.currency })
    setText(text.name)
    setSuggestions([])
  };


  const onSubmit = (e) => {
    dispatch(addAction(infoComplete))
      .then(response => setText(''))
      .catch(() => alert.show('Error al agregar una accion'))

    setDataTable([...dataTable, infoComplete])
  };


  useEffect(() => {
    dispatch(getActions()).then((response) => setDownloadData(response.action.payload.request.data))
    dispatch(getMyActions()).then((response) => setDataTable(response.action.payload.myActions))

  }, [])


  return (
    <div style={styles.container}>
      <div style={styles.nav}>
        <div style={styles.titleContainer}>
          <p style={styles.subtitle}>MIS ACCIONES</p>
        </div>

        <div style={styles.userLoginContainer}>
          <UserAccount />
        </div>

      </div>

      <form onSubmit={handleSubmit(onSubmit)}>
        <div style={styles.simbolContainer}>
          <div style={styles.columnContainer}>
            <input {...register("name")} onChange={e => handleChange(e.target.value)} value={text} style={styles.input} />
            <div style={styles.autoContainer}>
              {suggestions && suggestions.map((suggestion, index) => (
                <div key={index} onClick={() => handleSuggestions({ symbol: suggestion.symbol, name: suggestion.name, currency: suggestion.currency })}>
                  {suggestion.name}
                </div>
              ))}
            </div>
          </div>
          <div style={styles.buttonContainer}>
            <Button type="submit" styles={styles.button} text="Agregar Simbolo" />
          </div>
        </div>
      </form>

      <Table dataTable={dataTable} />

    </div>
  )
};

export { Dashboard };