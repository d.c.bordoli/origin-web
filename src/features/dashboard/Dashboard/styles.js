const styles = {
  container: {
    display: "flex",
    flex: 1,
    flexDirection: 'column',
    padding: 50,
    background: '#212332',
  },
  nav: {
    display: "flex",
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
    background: '#212332',
  },

  userLoginContainer: {
    display: "flex",
    flex: 0.5,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    backgroundColor: '#2A2D3E',
  },
  titleContainer: {
    display: "flex",
    flex: 0.5,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    backgroundColor: '#2A2D3E',
  },

  simbolContainer: {
    display: "flex",
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: '#212332',
    padding: 50
  },

  input: {
    width: 392,
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    color: '#666666',
    padding: 20
  },

  autoContainer: {
    backgroundColor: 'whitesmoke',
    width: 392,
  },

  subtitle: {
    fontSize: 12,
    fontWeight: 700,
    fontFamily: 'montserrat',
    color: '#858BA6',
  },

  buttonContainer:{
    alignSelf: 'flex-start'
  },
  button: {
    width: 233,
    height: 31,
    borderRadius: 10,
    backgroundColor: '#47C8B0',
    color: '#FFFFFF',
    fontWeight: 700,
    fontFamily: 'Arial',
    cursor: 'pointer',
    marginLeft: 40,
    marginTop: 20
  },
}

export { styles }