import React, { useEffect, useState } from 'react';
import { styles } from './styles';
import { UserAccount, Button } from '../../../components'
import { getActionDetail, getActionDetailRealTime, getActionDetailHistorical } from '../../../redux/actions'
import { useParams, useHistory } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'



const ActionDetail = () => {
  const dispatch = useDispatch()
  const infoState = useSelector((state) => state.twelveDataReducer);
  const { symbol } = useParams()
  const [actionDetail, setActionDetail] = useState()
  const [realTimeDetail, setRealTimeDetail] = useState([])
  const [historicalDetail, setHistoricalDetail] = useState([])
  const [parameters, setParameters] = useState({ checked: null })
  const [interval, setInterval] = useState({ checked: '1min' })
  const [startDate, setStartDate] = useState('')
  const [endDate, setEndDate] = useState('')
  const history = useHistory();


  let valueClose = []
  let dateTimeArr = []

  const options = {
    title: {
      text: 'Tiempo Real'
    },
    xAxis: {
      categories: dateTimeArr,
    },
    series: [{
      data: valueClose
    }],


    yAxis: {
      title: {
        text: 'Valores'
      },
    },
  }

  const handleChange = (e) => {
    setParameters({ checked: e.target.value })
  }

  const handleChangeIntervalo = (e) => {
    setParameters({ checked: null })
    setInterval({ checked: e.target.value })
  }

  const handleChangeStartDate = (e) => {
    setParameters({ checked: null })
    setStartDate(e.target.value)

  }

  const handleChangeEndDate = (e) => {
    setParameters({ checked: null })

    setEndDate(e.target.value)
  }

  useEffect(() => {
    if (parameters.checked === 'real') {
      dispatch(getActionDetailRealTime(symbol, interval.checked))
        .then(response => setRealTimeDetail(response.action.payload.request.values))
    }

    if (parameters.checked === 'historico') {
      dispatch(getActionDetailHistorical(symbol, startDate, endDate))
        .then(response => setHistoricalDetail(response.action.payload.request.values))
    }


  }, [parameters.checked])


  useEffect(() => {
    dispatch(getActionDetail(symbol))
      .then(response => setActionDetail(response.action.payload.request.data[0]))
  }, [])

  useEffect(() => {
  }, [valueClose])

  useEffect(() => {
  }, [historicalDetail])

  useEffect(() => {
  }, [dateTimeArr])

  useEffect(() => {
  }, [startDate])

  useEffect(() => {
  }, [endDate])



  return (
    <div style={styles.container}>
      <div style={styles.nav}>
        <div style={styles.titleContainer}>
          {actionDetail ? <p style={styles.subtitle}> {actionDetail.symbol} - {actionDetail.name} - {actionDetail.currency}</p> : null}
        </div>

        <div style={styles.userLoginContainer}>
          <UserAccount />
        </div>
      </div>
      <Button styles={styles.button} onClick={() => history.goBack()} text="Volver" />

      <div style={styles.radioContainer}>
        <div style={{ flexDirection: 'row' }}>

          <label style={styles.labelContainer}>
            <div style={{ flexDirection: 'column' }}>
              <input type='radio' value='1min' checked={interval.checked === '1min'} onChange={handleChangeIntervalo} />1 Minuto
              <input type='radio' value='5min' checked={interval.checked === '5min'} onChange={handleChangeIntervalo} />5 Minutos
              <input type='radio' value='15min' checked={interval.checked === '15min'} onChange={handleChangeIntervalo} />15 Minutos
            </div>
          </label>
        </div>

        <label style={styles.labelContainer}>
          <input type='radio' value='real' checked={parameters.checked === 'real'} onChange={handleChange} />Tiempo Real
        </label>

        <label style={styles.labelContainer}>
          Desde<input type="date" id="startDate" name="startDate" onChange={handleChangeStartDate} />
          Hasta<input type="date" id="endDate" name="endDate" onChange={handleChangeEndDate} />
        </label>


        <label style={styles.labelContainer}>
          <input type='radio' value='historico' checked={parameters.checked === 'historico'} onChange={handleChange} />Historico
        </label>
      </div>


      <div>
        {realTimeDetail && parameters.checked === 'real' &&
          realTimeDetail.map((value) => (
            valueClose.push(parseFloat(value.close)),
            dateTimeArr.push(value.datetime)
          ))}

        {historicalDetail && parameters.checked === 'historico' &&
          historicalDetail.map((value) => (
            valueClose.push(parseFloat(value.close)),
            dateTimeArr.push(value.datetime)
          ))}



        {parameters.checked === 'real' &&
          <HighchartsReact
            highcharts={Highcharts}
            options={options}
          />
        }

        {parameters.checked === 'historico' &&
          <HighchartsReact
            highcharts={Highcharts}
            options={options}
          />
        }


      </div>

    </div>

  )
};

export { ActionDetail };