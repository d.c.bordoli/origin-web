import * as React from "react"

const Arrow = (props) => {
  return (
    <svg
      fill="none"
      height={6}
      width={9}
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M0 1.058L1.058 0l3.43 3.491L7.913 0l1.058 1.058-4.485 4.55L0 1.057z"
        fill="#858BA6"
      />
    </svg>
  )
}

export { Arrow };
