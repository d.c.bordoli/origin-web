import * as React from "react"

const ETH = (props) => {
  return (
    <svg
      fill="none"
      height={31}
      width={19}
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <g clipPath="url(#prefix__clip0)">
        <path d="M9.5 0v11.405L0 15.606 9.5 0z" fill="#8693B0" />
        <path d="M9.5 11.405v9.916L0 15.606l9.5-4.201z" fill="#5C6B8D" />
        <path d="M9.5 23.36V31L0 17.807l9.5 5.553z" fill="#8693B0" />
        <path d="M9.5 0v11.405l9.5 4.201L9.5 0z" fill="#5C6B8D" />
        <path d="M9.5 11.405v9.916l9.5-5.715-9.5-4.201z" fill="#3D4B72" />
        <path d="M9.5 23.36V31L19 17.807 9.5 23.36z" fill="#5C6B8D" />
      </g>
      <defs>
        <clipPath id="prefix__clip0">
          <path d="M0 0h19v31H0z" fill="#fff" />
        </clipPath>
      </defs>
    </svg>
  )
}

export { ETH };
