const Cup = (props) => {
  return (
    <svg height={1} width={48} xmlns="http://www.w3.org/2000/svg" {...props}>
      <path d="M0 0h48v1H0z" fill="#063855" fillRule="evenodd" />
    </svg>
  )
}

export { Cup }
