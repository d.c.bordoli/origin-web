import * as React from "react"

const SpanishFlag = (props) => {
  return (
    <svg
      fill="none"
      height={29}
      width={29}
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M14.5 0A14.725 14.725 0 001.657 7.457h25.686A14.725 14.725 0 0014.5 0zM14.5 29c5.565 0 10.391-3.028 12.843-7.457H1.657C4.11 25.972 8.935 29 14.5 29z"
        fill="#D63932"
      />
      <path
        d="M27.183 7.457H1.817A14.747 14.747 0 000 14.578c0 2.504.619 4.891 1.74 6.965h25.52A14.63 14.63 0 0029 14.578c0-2.582-.657-5.008-1.817-7.12z"
        fill="#EABF07"
      />
    </svg>
  )
}

export { SpanishFlag }
