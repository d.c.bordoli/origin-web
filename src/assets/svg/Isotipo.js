import * as React from "react"

const Isotipo = (props) => {
  return (
    <svg
      fill="none"
      height={39}
      width={35}
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M34.408 10.141L18.138.405 17.5 0l-.671.405L.592 10.119 0 10.447v18.707l.614.34 16.374 9.167.683.339.58-.34 16.135-9.167L35 29.1V10.524l-.592-.383zm-16.93-7.319l13.688 8.194-8 3.753-5.006-2.845-.592-.317-.591.317-5.041 2.867-8.09-3.797 13.631-8.172zm-1.105 32.732l-13.881-7.79V13.03l8.272 3.873v5.71l.011.722.524.307 5.086 2.888v9.025h-.012zm1.195-11.126l-4.324-2.45v-5.164l4.324-2.45 4.324 2.461v5.153l-4.324 2.45zm1.297 2.046l4.973-2.833.523-.361.012-.668V16.87l8.135-3.818v14.725l-13.654 7.756v-9.058h.011z"
        fill="#fff"
      />
    </svg>
  )
}

export { Isotipo };
