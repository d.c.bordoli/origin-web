import * as React from "react"

const Close = (props) => {
  return (
    <svg
      fill="none"
      height={9}
      width={9}
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M6.205 4.5l2.56-2.559a.804.804 0 000-1.138L8.196.236a.804.804 0 00-1.138 0L4.5 2.795 1.941.235a.8.8 0 00-1.135 0l-.57.568a.804.804 0 000 1.138L2.795 4.5.235 7.059a.804.804 0 000 1.138l.568.567a.804.804 0 001.138 0L4.5 6.205l2.559 2.56a.804.804 0 001.138 0l.567-.568a.804.804 0 000-1.138L6.205 4.5z"
        fill="#858BA6"
      />
    </svg>
  )
}

export { Close };
