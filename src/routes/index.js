import React from "react";
import { useSelector } from 'react-redux';
import { Switch } from "react-router-dom";

import { PublicRoutes } from './publicRoutes';
import { Privateroutes } from './privateRoutes';

const Routes = () => {
  const { token } = useSelector((state) => state.autheticationReducer);
  const authState = useSelector((state) => state.autheticationReducer);
  console.log('authState', authState)


  return (
    <Switch>
      {token ? <Privateroutes /> : <PublicRoutes />}
    </Switch >
  );
};

export { Routes };
