import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import { LoginView, RegisterView } from "../features";

const routes = [
  {
    path: "/login",
    component: LoginView,
  },
  {
    path: "/register",
    component: RegisterView,
  },
];

const PublicRoutes = () => (
  <Switch>
    {routes.map((route, i) => (
      <Route exact key={i} {...route} />
    ))}

    <Route exact path="*">
      <Redirect to="/login" />
    </Route>
  </Switch >
);

export { PublicRoutes };
