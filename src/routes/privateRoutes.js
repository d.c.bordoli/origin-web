import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { useSelector } from "react-redux";
import axios from "axios";

import { Dashboard, ActionDetail } from '../features'

const routes = [
  {
    path: "/dashboard",
    component: Dashboard,
  },
  {
    path: "/actiondetail",
    component: ActionDetail,
  }


];

const Privateroutes = () => {
  const { token } = useSelector((state) => state.autheticationReducer)

  if (token)
    axios.defaults.headers = {
      "auth-token": token,
    }

  return (

    <Switch>
      {routes.map((route, i) => <Route exact key={i} {...route} />)}
      <Route exact path="/dashboard/" component={Dashboard} />
      <Route exact path="/actiondetail/:symbol" component={ActionDetail} />
      <Route exact path="*">

        <Redirect to="/dashboard" />
      </Route>
    </Switch >
  )
};

export { Privateroutes };
